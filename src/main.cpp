#include <caf/all.hpp>

struct getQuantity {};
struct add {};
struct get {};
struct getAllProducts {};
struct addGarlic {};
struct addOnion {};
struct addSalt {};
struct addSugar {};
struct addOil {};
struct getGarlic {};
struct getOnion {};
struct getSalt {};
struct getSugar {};
struct getOil {};


struct PState {
  int quantity;
};

caf::behavior product(caf::stateful_actor<PState>* self) {
  self -> state.quantity = 0;

  return caf::behavior{
    [self](add, int q) {
      self->state.quantity += q; 
    },

    [self](get, int q) {
      self->state.quantity -= q; 
    },

    [self](getQuantity) {
      return self->state.quantity; 
    }
  };
}

struct ShopState {
  caf::actor_system_config cfg;
  caf::actor_system system{cfg};

  caf::actor garlic = system.spawn(product);
  caf::actor onion = system.spawn(product);
  caf::actor salt = system.spawn(product);
  caf::actor sugar = system.spawn(product);
  caf::actor oil = system.spawn(product);
};

caf::behavior shop(caf::stateful_actor<ShopState>* self) {
  
  return caf::behavior{
    [self](getAllProducts) {
      aout(self) << "Products:" << std::endl;

      self->request(self->state.garlic, caf::infinite, getQuantity{}).await([self](int q) {
        aout(self) << "Garlic [Quantity - kg]: " << q << std::endl;
      });

      self->request(self->state.oil, caf::infinite, getQuantity{}).await([self](int q) {
        aout(self) << "Oil [Quantity - l]: " << q << std::endl;
      });

      self->request(self->state.onion, caf::infinite, getQuantity{}).await([self](int q) {
        aout(self) << "Onion [Quantity - kg]: " << q << std::endl;
      });

      self->request(self->state.salt, caf::infinite, getQuantity{}).await([self](int q) {
        aout(self) << "Salt [Quantity - kg]: " << q << std::endl;
      });

      self->request(self->state.sugar, caf::infinite, getQuantity{}).await([self](int q) {
        aout(self) << "Sugar [Quantity - kg]: " << q << std::endl;
      });

    },

    [self](addGarlic, int q) {
      self->send(self->state.garlic, add{}, q);
    },

    [self](addOnion, int q) {
      self->send(self->state.onion, add{}, q);
    },

    [self](addSalt, int q) {
      self->send(self->state.salt, add{}, q);
    },

    [self](addSugar, int q) {
      self->send(self->state.sugar, add{}, q);
    },

    [self](addOil, int q) {
      self->send(self->state.oil, add{}, q);
    },

    [self](getGarlic, int q) {
      self->send(self->state.garlic, get{}, q);
    },

    [self](getOnion, int q) {
      self->send(self->state.onion, get{}, q);
    },

    [self](getSalt, int q) {
      self->send(self->state.salt, get{}, q);
    },

    [self](getSugar, int q) {
      self->send(self->state.sugar, get{}, q);
    },

    [self](getOil, int q) {
      self->send(self->state.oil, get{}, q);
    }

  };
}

int main() {
  caf::actor_system_config cfg;
  caf::actor_system system{cfg};

  caf::actor shopActor = system.spawn(shop);
  
  caf::anon_send(shopActor, addGarlic{}, 10);
  caf::anon_send(shopActor, addOnion{}, 5);
  caf::anon_send(shopActor, addSalt{}, 10);
  caf::anon_send(shopActor, addSugar{}, 10);
  caf::anon_send(shopActor, addOil{}, 10);

  caf::anon_send(shopActor, getAllProducts{});
  
  caf::anon_send(shopActor, getGarlic{}, 5);
  caf::anon_send(shopActor, getOnion{}, 2);
  caf::anon_send(shopActor, getSalt{}, 7);
  caf::anon_send(shopActor, getSugar{}, 6);
  caf::anon_send(shopActor, getOil{}, 4);

  caf::anon_send(shopActor, getAllProducts{});
  
  return 0;
}
